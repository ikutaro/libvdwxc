.. _configuration:
.. index:: Authors, Contributors

Authors and contributors
************************************

:program:`libvdwxc` was primarily written by Ask Hjorth Larsen [#em1]_ [#ad1]_
and Mikael Kuisma [#em2]_ [#ad2]_ [#ad3]_ [#ad4]_ with contributions from
Joakim Löfgren [#ad2]_, Yann Pouillon [#ad1]_, Paul Erhart [#ad2]_,
and Per Hyldgaard [#ad3]_.


.. rubric:: Footnotes

.. [#em1] `<asklarsen@gmail.com>`_
.. [#em2] `<mikael.j.kuisma@jyu.fi>`_
.. [#ad1] Nano-bio Spectroscopy Group and ETSF Scientific development centre, Universidad del País Vasco UPV/EHU, Spain
.. [#ad2] Department of Physics, Chalmers University of Technology, Sweden
.. [#ad3] Department of Microtechnology and Nanoscience, Chalmers University of Technology, Sweden
.. [#ad4] Department of Chemistry, University of Jyväskylä, Jyväskylä, Finland
