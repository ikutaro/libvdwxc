#ifndef VDW_KERNEL_H
#define VDW_KERNEL_H

struct vdwxc_kernel
{
    int nalpha;
    int nregions;
    int npalphacoefs;
    int nkernelcoefs;
    int nkpoints;
    double deltak;

    // qmesh indices: a (~20 alpha)
    double *qmesh_a;
    // spline indices: a (~20 alpha) r (~19 regions) c (~4 spline coefficients)
    double *splines_arc;
    // kernel indices: aa (~20x20 alpha) k (~1000 points) c (~4 spline coefs)
    double *kernel_aakc;
};

// Input:
//   double k: interpolation point
// Output:
//   double kernel_aa[20*20]: value of kernels at the interpolation point
void vdwxc_interpolate_kernels(struct vdwxc_kernel *kernel,
                               double k, double* kernel_aa);


struct vdwxc_kernel vdwxc_new_kernel(int nalpha, int nregions,
                                     int npalphacoefs, int nkernelcoefs,
                                     int nkpoints,
                                     double deltak,
                                     double *qmesh_a, double *splines_arc,
                                     double *kernel_aakc);

struct vdwxc_kernel vdwxc_default_kernel();

void vdwxc_evaluate_palpha_splines(struct vdwxc_kernel *kernel,
                                   double prefactor, double q, double* output);
void vdwxc_evaluate_palpha_splines_derivative(struct vdwxc_kernel *kernel,
                                              double q0, double* output);

#endif
