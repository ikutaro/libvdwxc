from gpaw import GPAW, Davidson
from ase import Atoms
from gpaw.xc.libvdwxc import VDWDF
from gpaw.utilities import h2gpts
import numpy as np
from gpaw.mpi import rank

class XX(VDWDF):
    def calculate_nonlocal(self, n_g, sigma_g, v_g, dedsigma_g):
        energy = VDWDF.calculate_nonlocal(self, n_g, sigma_g, v_g, dedsigma_g)
        if rank == 0:
            np.savetxt('rho.txt', n_g[0, 0, :])
            np.savetxt('sigma.txt', sigma_g[0, 0, :])
            np.savetxt('potential.txt', v_g[0, 0, :])
            np.savetxt('dedsigma.txt', dedsigma_g[0, 0, :])
        return energy

system = Atoms('Ne')
system.pbc = 1
system.center(vacuum=8.0)
system.positions[:] = 0.0

calc = GPAW(xc=XX(),
            eigensolver=Davidson(10),
            txt='gpaw.txt',
            gpts=h2gpts(0.12, system.get_cell(), idiv=8))

system.set_calculator(calc)
system.get_potential_energy()
