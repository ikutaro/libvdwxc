#ifndef VDW_RADIAL_H
#define VDW_RADIAL_H

#include "vdwxc.h"

#define VDW_FBT_FORWARD 1
#define VDW_FBT_BACKWARD 2


void vdwxc_fourier_bessel_transform(int Ninput, int Noutput,
                                    int lda_input, int lda_output,
                                    double dr, int premul_input,
                                    double* input, double* output,
                                    int direction);

// Debug function, to test the spherical Bessel transform
double vdwxc_calculate_radial_hartree(int N, double dr, double* rho_i, double* dedn_i);
#endif
