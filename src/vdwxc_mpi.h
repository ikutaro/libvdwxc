#ifndef VDWXC_MPI_H
#define VDWXC_MPI_H


#ifdef __cplusplus
extern "C" {
#endif

#include "vdwxc.h"
#include <mpi.h>


#define VDWXC_HAS_MPI

void vdwxc_set_communicator(vdwxc_data data, MPI_Comm mpi_comm);
void vdwxc_init_mpi(vdwxc_data data, MPI_Comm mpi_comm);
void vdwxc_init_pfft(vdwxc_data data, MPI_Comm mpi_comm, int nproc1, int nproc2);

#ifdef __cplusplus
}
#endif
#endif
